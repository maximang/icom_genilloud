<?php
require('inc/security.php');

if(isset($_GET['id']) && is_numeric($_GET['id']))
{
  require_once('inc/config.php');
  $query = $db -> prepare('DELETE FROM project WHERE id = ?');
  $query -> execute(array($_GET['id']));
}

header('Location:index.php');
?>
