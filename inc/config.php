<?php
$dbtype = 'mysql';
$dbhost = 'localhost';
$dbname = 'projectmng';
$dbuser = 'root';
$dbpswd = '';

try {
  $db = new PDO($dbtype.':host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpswd);
}
catch(Exception $e) {
  echo($e -> getMessage());
  exit();
}
?>
