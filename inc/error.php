<?php
if(isset($_SESSION['error'])) {
	foreach($_SESSION['error'] as $error):
?>
		<p class="alert callout" data-closable="fade-out">
			<?php echo $error; ?>
			<button class="close-button" aria-label="Fermer" type="button" data-close>
				<span aria-hidden="true">&times;</span>
			</button>
		</p>
<?php
	endforeach;
	unset($_SESSION['error']);
}
?>
