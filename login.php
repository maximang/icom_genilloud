<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <?php
		$title = 'Connexion';
		require('template/head.php');
		?>
  </head>
  <body>
    <div class="container">
			<div class="row column small-4">
				<form method="post" action="logme.php">
					<label for="username">Nom d'utilisateur</label>
					<input type="text" name="username" id="username" placeholder="Nom d'utilisation" />
					<label for="password">Mot de passe</label>
					<input type="password" name="password" id="password" />
					<input type="submit" name="submit" class="button" value="Connexion" />
				</form>
			</div>
    </div>

		<?php require('template/footer.php');	?>
  </body>
</html>
