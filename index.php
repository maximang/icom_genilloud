<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <?php require('template/head.php');	?>
  </head>
  <body>
		<?php require('template/header.php');	?>

    <div class="container">
			<ul class="project">
				<li class="project-header">
					<span class="project-item-name">&nbsp;</span>
					<span class="project-item-month">Janvier</span>
					<span class="project-item-month">Février</span>
					<span class="project-item-month">Mars</span>
					<span class="project-item-month">Avril</span>
					<span class="project-item-month">Mai</span>
					<span class="project-item-month">Juin</span>
					<span class="project-item-month">Juillet</span>
					<span class="project-item-month">Août</span>
					<span class="project-item-month">Septembre</span>
					<span class="project-item-month">Octobre</span>
					<span class="project-item-month">Novembre</span>
					<span class="project-item-month">Décembre</span>
					<span class="project-item-action">&nbsp;</span>
				</li>
				<?php
				function displayDateBadge($startDate, $endDate, $month) {
					$display = '';
					if(date('m', strtotime($startDate)) == $month)
						$display .= '<span class="badge success">&nbsp;</span>';
					if(date('m', strtotime($endDate)) == $month)
						$display .= '<span class="badge alert">&nbsp;</span>';

					if(!empty($display)) echo $display;
					else echo '&nbsp;';
				}

				$query = $db -> query('SELECT * FROM project');
				while($data =	$query -> fetch()):
				?>
				<li class="project-item">
					<span class="project-item-name"><?php echo $data['name'];	?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '01'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '02'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '03'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '04'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '05'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '06'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '07'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '08'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '09'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '10'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '11'); ?></span>
					<span class="project-item-month"><?php displayDateBadge($data['start'], $data['end'], '12'); ?></span>
					<span class="project-item-action"><a href="delete.php?id=<?php echo $data['id'];	?>">Supprimer</a></span>
				</li>
				<?php endwhile;	?>
			</ul>
    </div>

		<a href="add.php" class="project-add">Ajouter un projet</a>

		<?php require('template/footer.php');	?>
  </body>
</html>
