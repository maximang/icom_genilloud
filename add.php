<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <?php
		$title = 'Ajouter un projet';
		require('template/head.php');
		?>
  </head>
  <body>
		<?php require('template/header.php');	?>

    <div class="container">
			<div class="row column small-4">
				<?php include('inc/error.php'); ?>
				<form method="post" action="add-action.php">
					<label for="name">Nom du projet</label>
					<input type="text" name="name" id="name" placeholder="Nom du projet" />
					<label for="start">Date de début du projet</label>
					<input type="date" name="start" id="start" value="<?php echo date('Y-m-d'); ?>" />
					<label for="end">Date de fin du projet</label>
					<input type="date" name="end" id="end" value="<?php echo date('Y-m-d'); ?>" />
					<input type="submit" name="submit" class="button" value="Ajouter" />
				</form>
			</div>
    </div>

		<?php require('template/footer.php');	?>
  </body>
</html>
