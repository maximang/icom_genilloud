<?php
require_once('inc/security.php');
require_once('inc/config.php');

if(isset($_POST['submit'])) {
  $name = $_POST['name'];
  $start = $_POST['start'];
  $end = $_POST['end'];

  if(empty($name)) $error[] = 'Le champ "Nom du projet" est vide.';
  if(empty($start)) $error[] = 'Le champ "Date de début du projet" est vide.';
  if(empty($end)) $error[] = 'Le champ "Date de fin du projet" est vide.';
  if(!strtotime($start)) $error[] = 'Le champ "Date de début du projet" contient une valeur invalide.';
  if(!strtotime($end)) $error[] = 'Le champ "Date de fin du projet" contient une valeur invalide.';
	if(strtotime($start) > strtotime($end)) $error[] = 'Le projet ne peut pas finir avant d\'avoir commencé';

  if(isset($error)) {
    $_SESSION['error'] =	$error;

    header('Location:add.php');
    exit();
  }

  $query = $db -> prepare('INSERT INTO project(name, start, end) VALUES(?, ?, ?)');
  $query -> execute(array($name, $start, $end));
}

header('Location:index.php');
?>
